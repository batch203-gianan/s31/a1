/*

7. Create a server using the createServer method that will listen in to the port provided above.
8. Console log in the terminal a message when the server is successfully running.
9. Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
10. Access the login route to test if it’s working as intended.
11. Create a condition for any other routes that will return an error message.
12. Access any other route to test if it’s working as intended.
*/

const http = require("http");

const port = 3000;

const server = http.createServer(function(request,response){
	if(request.url == "/login"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to the login page.");
	}
	else if(request.url == "/register"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("I'm sorry the page you are looking for cannot be found.");
	} 
	else {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end(`Hello, this is the homepage. Kindly add in the address bar "/login" or "/register" to navigate the site.`);
	}
}).listen(3000);

console.log(`THe server is running on port: ${port}`);